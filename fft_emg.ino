#include <arduinoFFT.h>
#include <Servo.h>

int SAMPLES = 128; // Number of samples
const int AVESAMPLES = 5;
double ave[AVESAMPLES];

arduinoFFT FFT = arduinoFFT();
Servo servo;

int EMG_PIN = A0;
int SERVO_PIN = 8;
int sensor_value = 0;

void setup() {
  for (int i=0; i<AVESAMPLES; i++) {
    ave[i] = 5;  
  }
  servo.attach(SERVO_PIN);
  pinMode(EMG_PIN, INPUT);
  Serial.begin(115200);
}

void loop() {
  double real_array[SAMPLES];
  double imaginary_array[SAMPLES];

  servo.write(90);
  bool trigger = false;
  // Collect and store SAMPLES of data into vReal
  long int before = millis();
  for (int i=0; i<SAMPLES; i++) {
    sensor_value = analogRead(EMG_PIN);
    real_array[i] = sensor_value;
    imaginary_array[i] = 0.0;
  }  
  //Find the frequencies of interest
  Serial.print("\Input: ");
  for (int i=0; i<SAMPLES; i++) {
    Serial.print(real_array[i]);
    Serial.print(" | ");
  }

  long int after = millis();
  double sampling_frequency = (1000.0*SAMPLES)/(after-before);
  Serial.print("\nSAMPLE F: ");
  Serial.print(sampling_frequency);
  Serial.print("\n");

  // Remove DC component from signal
  FFT.DCRemoval();

  // Apply a blackman window function to real_array
  FFT.Windowing(real_array, SAMPLES, FFT_WIN_TYP_BLACKMAN, FFT_FORWARD);

  // Perform the FFT
  FFT.Compute(real_array, imaginary_array, SAMPLES, FFT_FORWARD);
  FFT.ComplexToMagnitude(real_array, imaginary_array, SAMPLES);

  Serial.print("\nOutput: ");
  for (int i=0; i<SAMPLES+1; i++) {
    double freq = (sampling_frequency / SAMPLES) * i;
    Serial.print(freq);
    Serial.print(" ");
    Serial.print(real_array[i]);
    Serial.print(" | ");
  }

  int FREQUENCY_ARRAY_MIN = 3; // 30 Hz
  shiftAve(real_array[FREQUENCY_ARRAY_MIN]);

  double Threshold = threshold();

  Serial.print("\nThreshold: ");
  Serial.println(Threshold);

  if (real_array[FREQUENCY_ARRAY_MIN] > Threshold){
    trigger = true;
  }

  if (trigger == true){
    Serial.print("\n------------------trigger hit-------------/n");
    servo.write(0);
    delay(500);
  }
}

double threshold(){
  double sum = 0;
  for (int i=0; i<AVESAMPLES; i++){
    sum += ave[i];
  }
  return (sum / AVESAMPLES) * 2; // trial and error
}

void shiftAve(double newVal){
  for (int i=AVESAMPLES-1; i>0; i--){
    ave[i] = ave[i-1];
  }
  ave[0] = newVal;
}